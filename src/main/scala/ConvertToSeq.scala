import java.io.StringReader

import com.springer.semantic.classifier.utils.StopWords
import org.apache.lucene.analysis.core._
import org.apache.lucene.analysis.shingle.ShingleAnalyzerWrapper
import org.apache.lucene.analysis.standard.StandardTokenizer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.lucene.analysis.{Analyzer, TokenStream, Tokenizer}
import org.apache.spark.mllib.feature.{Word2VecModel, Word2Vec}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

object ConvertToSeq {
  //  private[utils] val stopWords: util.List[String] = Arrays.asList("a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with", "we", "were", "has", "between", "from", "using", "have", "we", "which", "null", "most", "one", "more", "1", "p", "used", "compared", "can", "two", "high", "than", "other", "our", "well", "been", "during", "found", "both", "from", "have", "associated", "may", "between", "time", "all", "however", "after", "background", "study", "also", "had", "using", "g", "its", "under", "important", "significant", "based", "methods", "results", "studies", "use", "non", "expression", "different", "significantly", "only", "null", "batch", "growth", "type", "levels", "increased", "subjects", "rating", "total", "scores", "scale", "areas", "site", "set", "sets", "activity", "left", "right", "vs", "nm", "among", "who", "underwent", "scores", "items", "self", "expressed", "expression", "us", "per", "costs", "based", "va", "show", "within", "qtl", "snp", "recurrence", "showed", "case", "cases", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "type", "here", "further", "could", "would", "sec", "st", "data", "identified", "years", "specific", "related", "including", "first", "three", "higher", "analysis", "present", "number", "production", "ci", "0,0", "recent", "ra", "anti", "3d", "7bit", "a0", "about", "above", "abstract", "across", "additional", "after", "conclusions", "afterwards", "again", "against", "align", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "arial", "around", "as", "ascii", "assert", "at", "back", "background", "base64", "bcc", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bgcolor", "blank", "blockquote", "body", "boolean", "border", "both", "br", "break", "but", "by", "can", "cannot", "cant", "case", "catch", "cc", "cellpadding", "cellspacing", "center", "char", "charset", "cheers", "class", "co", "color", "colspan", "com", "con", "const", "continue", "could", "couldnt", "cry", "css", "de", "dear", "default", "did", "didnt", "different", "div", "do", "does", "doesnt", "done", "dont", "double", "down", "due", "during", "each", "eg", "eight", "either", "else", "elsewhere", "empty", "encoding", "enough", "enum", "etc", "eu", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "extends", "face", "family", "few", "ffffff", "final", "finally", "float", "font", "for", "former", "formerly", "fri", "from", "further", "get", "give", "go", "good", "got", "goto", "gt", "h1", "ha", "had", "has", "hasnt", "have", "he", "head", "height", "hello", "helvetica", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "hi", "him", "himself", "his", "how", "however", "hr", "href", "html", "http", "https", "id", "ie", "if", "ill", "im", "image", "img", "implements", "import", "in", "inc", "instanceof", "int", "interface", "into", "is", "isnt", "iso-8859-1", "it", "its", "itself", "ive", "just", "keep", "last", "latter", "latterly", "least", "left", "less", "li", "like", "long", "look", "lt", "ltd", "mail", "mailto", "many", "margin", "may", "me", "meanwhile", "message", "meta", "might", "mill", "mine", "mon", "more", "moreover", "most", "mostly", "mshtml", "mso", "much", "must", "my", "myself", "name", "namely", "native", "nbsp", "need", "neither", "never", "nevertheless", "new", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "null", "of", "off", "often", "ok", "on", "once", "only", "onto", "or", "org", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "package", "pad", "per", "perhaps", "plain", "please", "pm", "printable", "private", "protected", "public", "put", "quot", "quote", "r1", "r2", "rather", "re", "really", "regards", "reply", "return", "right", "said", "same", "sans", "sat", "say", "saying", "see", "seem", "seemed", "seeming", "seems", "serif", "serious", "several", "she", "short", "should", "show", "side", "since", "sincere", "six", "sixty", "size", "so", "solid", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "span", "src", "static", "still", "strictfp", "string", "strong", "style", "stylesheet", "subject", "such", "sun", "super", "sure", "switch", "synchronized", "table", "take", "target", "td", "text", "th", "than", "thanks", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thick", "thin", "think", "third", "this", "those", "though", "three", "through", "throughout", "throw", "throws", "thru", "thu", "thus", "tm", "to", "together", "too", "top", "toward", "towards", "tr", "transfer", "transient", "try", "tue", "type", "ul", "un", "under", "unsubscribe", "until", "up", "upon", "us", "use", "used", "uses", "using", "valign", "verdana", "very", "via", "void", "volatile", "want", "was", "we", "wed", "weight", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "width", "will", "with", "within", "without", "wont", "would", "wrote", "www", "yes", "yet", "you", "your", "yours", "yourself", "yourselves", "lc", "il", "response", "method", "throughput", "developed", "allows", "system", "gy", "months", "quality", "given", "available", "low", "observed", "disease", "potential", "95", "art", "mm3", "df")
  //  val stopSet: CharArraySet = new CharArraySet(stopWords, false)

  //  val sparkConf: SparkConf = new SparkConf().setAppName("JavaWordCount").setMaster("local[2]").set("spark.executor.memory", "1g");
  val sparkConf = new SparkConf()
    .setMaster("local[2]")
    .setAppName("Word2Vec")
    .set("spark.executor.memory", "1g")
  val sc = new SparkContext(sparkConf)

  @throws(classOf[Exception])
  def main(args: Array[String]) {
    println("Hello, world!")
    System.out.println("Result:" + convert(sc))
  }

  def convert(sc: SparkContext) = {
    // Format Journal, ArticleID, Abstract
    val lines = sc.textFile("/Users/davidhorby/data/BMCAll-train.tsv")
    //    val csv: RDD[Array[String]] = lines.map(_.split("\t"))
    val dataset: RDD[(String, String)] = lines.map(x => (x.split("\t")(1), x.split("\t")(2)))
    val documents: RDD[Seq[Char]] = sc.textFile("/Users/davidhorby/data/BMCAll-train.tsv").map(_.split("\t")(2).toSeq)
    val result: (String, String) = dataset.first()
    println("Size:" + result._1 + ":" + result._2)
    val tokens: Seq[String] = tokenize(result._2)

    val input: RDD[Seq[String]] = sc.textFile("/Users/davidhorby/data/BMCAll-train.tsv").map(line => tokenize(line))

    val dd: Seq[String] = input.first()

    dd.foreach(println)

    val word2vec = new Word2Vec()

    val model: Word2VecModel = word2vec.fit(input)

    val synonyms2 = model.findSynonyms("influenza", 40)

    for ((synonym, cosineSimilarity) <- synonyms2) {
      println(s"$synonym $cosineSimilarity")
    }

    val synonyms3 = model.findSynonyms("healthcare", 40)

    for ((synonym, cosineSimilarity) <- synonyms3) {
      println(s"$synonym $cosineSimilarity")
    }


    sc.parallelize(Seq(model), 1).saveAsObjectFile("/Users/davidhorby/data/bmcModelPath")
    val sameModel = sc.objectFile[Word2VecModel]("/Users/davidhorby/data/bmcModelPath").first()


  }

  def tokenize(content: String): Seq[String] = {
    val tReader = new StringReader(content)
    val shingleWrapper = new ShingleAnalyzerWrapper(new CustomAnalyzer, 2, 3)
    val tStream: TokenStream = shingleWrapper.tokenStream("contents", content)
    //    val tStream: TokenStream = analyzer.tokenStream("contents", tReader)
    val term = tStream.addAttribute(classOf[CharTermAttribute])
    tStream.reset()

    val result = mutable.ArrayBuffer.empty[String]
    while (tStream.incrementToken()) {
      val termValue = term.toString
      if (!(termValue matches ".*[\\d\\.].*")) {
        result += term.toString
      }
    }
    result
  }

  class CustomAnalyzer extends Analyzer {
    protected def createComponents(data: String): Analyzer.TokenStreamComponents = {
      val tokenizer: Tokenizer = new StandardTokenizer()
      val filter: StopFilter = new StopFilter(tokenizer, StopWords.stopSet)

//      val filter: TokenStream = new ShingleFilter(tokenizer, 2, 2)
      return new Analyzer.TokenStreamComponents(tokenizer, filter)
    }
  }

}


