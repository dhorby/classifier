package com.springer.semantic.classifier.utils

import org.apache.spark.mllib.classification.NaiveBayes
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel, LDA}
import org.apache.spark.mllib.feature.{IDFModel, IDF, HashingTF}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object TfIdfUtils {
  val sparkConf: SparkConf = new SparkConf().setAppName("JavaWordCount").setMaster("local[2]").set("spark.executor.memory", "1g")
  val sc = new SparkContext(sparkConf)

}

class TfIdfUtils {

  import TfIdfUtils._


  def createTfIdf(fileName:String): Unit = {

    // Load documents (one per line).
    val documents: RDD[Seq[String]] = sc.textFile(fileName).map(_.split(" ").toSeq)
    val terms = documents.flatMap(_.toList).distinct().collect().sortBy(identity)

    val splits: Array[RDD[Seq[String]]] = documents.randomSplit(Array(0.7, 0.3))
    val trainSet: RDD[Seq[String]] = splits(0)
    val testSet: RDD[Seq[String]] = splits(1)


    // Compute the IDF Vector
    val hashingTF = new HashingTF()
    val hashTrainSet: RDD[Vector] = hashingTF.transform(trainSet)
    hashTrainSet.cache()

    val idfModel: IDFModel = new IDF(minDocFreq = 3).fit(hashTrainSet)
    val tfidf: RDD[Vector] = idfModel.transform(hashTrainSet)
//    println("--->>>> Number fo vectors = " + tfidf.count())
//    tfidf.foreach(f => println( "HashCode:" + f.hashCode() + " Vectors:" + f.size + ":" + f.toArray.foreach(vv => println(" ---+--->" + vv.toString))))


    val train = hashTrainSet.map { point => LabeledPoint(point.hashCode(), idfModel.transform(point))  }
    train.cache()

    val hashTestSet: RDD[Vector] = hashingTF.transform(testSet)
    val test = hashTestSet.map{ point=> LabeledPoint(point.hashCode(),idfModel.transform(point)) }
    test.cache()

    val nbmodel = NaiveBayes.train(train, lambda = 1.0)
    val bayesTrain = train.map((p: LabeledPoint) => (nbmodel.predict(p.features), p.label))
    val bayesTest = test.map(p => (nbmodel.predict(p.features), p.label))
    val res: (Double, Double) = (bayesTrain.filter(x => x._1 == x._2).count() / bayesTrain.count().toDouble,
        bayesTest.filter(x => x._1 == x._2).count() / bayesTest.count().toDouble)
    println("Mean Naive Bayes performance:" + res.toString())

    //    // Scale the term frequencies
    //    tf.cache()
    //    val idf = new IDF(minDocFreq = 1).fit(tf)
    ////    val idf = new IDF(minDocFreq = 2).fit(tf)
    //    val tfidf: RDD[Vector] = idf.transform(tf)
    //    tfidf.foreach((vec: Vector) => println("+++>>>" + vec))
    //
    //    val splits = documents.randomSplit(Array(0.7, 0.3))
    //    val trainDocs = splits(0).map{ (x1: Seq[String]) =>x1.features}
    //    val idfModel = new IDF(minDocFreq = 3).fit(trainDocs)
    //    val train = splits(0).map{ point=>
    //      LabeledPoint(point.label,idfModel.transform(point.features))
    //    }
    //    val test = splits(1).map{ point=> LabeledPoint(point.label,idfModel.transform(point.features))
    //    }
    //    train.cache()

    //    tfidf.ra


    //    this.ldaCluster(tfidf)
    //
    //    tfidf.map((vec: Vector) => LabeledPoint(1.0, vec))


     //Train
//        val model = NaiveBayes.train(tfidf)
//
//        val data = mockData.union(watchData)
//        val config = new Configuration
//        config.setEnableImageFetching(false)
//        val goose = new Goose(config)
//        val content = goose.extractContent(url).cleanedArticleText
//        // tokenize content and stem it
//        val tokens = Tokenizer.tokenize(content)
//        // compute TFIDF vector
//        val tfIdfs = naiveBayesAndDictionaries.termDictionary.tfIdfs(tokens, naiveBayesAndDictionaries.idfs)
//        val vector = naiveBayesAndDictionaries.termDictionary.vectorize(tfIdfs)
//        // classify document
//        val labelId = model.predict(vector)
//        // convert label from double
//        println("Label: " + naiveBayesAndDictionaries.labelDictionary.valueOf(labelId.toInt))


  }



  def naiveBayes(fileName:String): Unit = {
    val data = sc.textFile("spark/test/naive_bayes/sample_naive_bayes_data.txt")
    val parsedData = data.map { line =>
      val parts = line.split(',')
      LabeledPoint(parts(0).toDouble, Vectors.dense(parts(1).split(' ').map(_.toDouble)))
    }

    // Split data into training (60%) and test (40%).
    val splits = parsedData.randomSplit(Array(0.6, 0.4), seed = 11L)
    val training = splits(0)
    val test = splits(1)

    val model = NaiveBayes.train(training, lambda = 1.0)
    val prediction = model.predict(test.map(_.features))

    val predictionAndLabel = prediction.zip(test.map(_.label))
    val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
  }

  def kMeans(tfidf: RDD[Vector]) = {

    // Cluster the data into two classes using KMeans
    val numIterations = 2
    val numClusters = 2
    val clusters: KMeansModel = KMeans.train(tfidf, numClusters, numIterations)

    // Evaluate clustering by computing Within Set Sum of Squared Errors
    val WSSSE = clusters.computeCost(tfidf)

    println("Within Set Sum of Squared Errors = " + WSSSE)
    println("Number of clusters = " + clusters.clusterCenters.size)
//    clusters.clusterCenters.foreach((vec: Vector) => println("dddd>>>" + vec.toString))

    // Save and load model
//    clusters.save(sc, "myModelPath")
//    val sameModel = KMeansModel.load(sc, "myModelPath")
  }

  def ldaCluster(tfidf: RDD[Vector]): Unit = {

    // Index documents with unique IDs
    val corpus = tfidf.zipWithIndex.map(_.swap).cache()

    // Cluster the documents into three topics using LDA
    val ldaModel = new LDA().setK(3).run(corpus)

    // Output topics. Each is a distribution over words (matching word count vectors)
    println("Learned topics (as distributions over vocab of " + ldaModel.vocabSize + " words):")
    val topics = ldaModel.topicsMatrix
    for (topic <- Range(0, 3)) {
      print("Topic " + topic + ":")
      for (word <- Range(0, ldaModel.vocabSize)) { print(" " + topics(word, topic)); }
      println()
    }
//
//    // Save and load model.
//    ldaModel.save(sc, "myLDAModel")
//    val sameModel = DistributedLDAModel.load(sc, "myLDAModel")
  }
}
