package com.springer.semantic.classifier.utils

import org.apache.spark.mllib.classification.{NaiveBayesModel, NaiveBayes}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.feature.{IDFModel, HashingTF, IDF}
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Models {


  def main(args: Array[String]) {
    val sparkConf: SparkConf = new SparkConf().setAppName("JavaWordCount").setMaster("local[2]").set("spark.executor.memory", "1g")
    val sc = new SparkContext(sparkConf)

    // read in data files (point to your location)
    val gilesFraser = sc.textFile("GilesFraser.txt")
    val testData = sc.textFile("test.txt")

    // convert data to numeric features with TF
    // we will consider Mockingbird passages = class 1, Watchman = class 0
    val tf = new HashingTF(10000)
    val mockData = gilesFraser.map { line =>
      var target = "1"
      LabeledPoint(target.toDouble, tf.transform(line.split(",")))
    }
    val testDataMap: RDD[LabeledPoint] = testData.map { line =>
      var target = "0"
      LabeledPoint(target.toDouble, tf.transform(line.split(" ")))
    }

    // build IDF model and transform data into modeling sets
//    val data = mockData.union(testDataMap)
    val splits: Array[RDD[LabeledPoint]] = mockData.randomSplit(Array(0.7, 0.3))
    val trainDocs: RDD[Vector] = splits(0).map { x => x.features }
    val idfModel: IDFModel = new IDF(minDocFreq = 3).fit(trainDocs)
    val train = splits(0).map { point =>
      LabeledPoint(point.label, idfModel.transform(point.features))
    }
    train.cache()
    val test: RDD[LabeledPoint] = splits(1).map { point =>
      LabeledPoint(point.label, idfModel.transform(point.features))
    }

    println("Number of training examples: ", train.count())
    println("Number of test examples: ", test.count())

    // NAIVE BAYES MODEL
    println("Training Naive Bayes Model...")
    val nbmodel: NaiveBayesModel = NaiveBayes.train(train, lambda = 1.0)
    val bayesTrain = train.map(p => (nbmodel.predict(p.features), p.label))
    val bayesTest = test.map(p => (nbmodel.predict(p.features), p.label))
    println("NB Training accuracy: ", bayesTrain.filter(x => x._1 == x._2).count() / bayesTrain.count().toDouble)
    println("NB Test set accuracy: ", bayesTest.filter(x => x._1 == x._2).count() / bayesTest.count().toDouble)
    println("Naive Bayes Confusion Matrix:")
    val count1: Long = bayesTest.filter(x => x._1 == 1.0 & x._2 == 1.0).count()
    val count2: Long = bayesTest.filter(x => x._1 == 0.0 & x._2 == 0.0).count()
    val count3: Long = bayesTest.filter(x => x._1 == 1.0 & x._2 == 0.0).count()
    val count4: Long = bayesTest.filter(x => x._1 == 0.0 & x._2 == 1.0).count()
    println("Predict:gilesFraser,label:gilesFraser -> ", count1)
    println(count2 + "," + count1)
    println(count3 + "," + count4)
    println("Predict:testData,label:testData -> ", bayesTest.filter(x => x._1 == 0.0 & x._2 == 0.0).count())
    println("Predict:gilesFraser,label:testData -> ", bayesTest.filter(x => x._1 == 1.0 & x._2 == 0.0).count())
    println("Predict:testData,label:gilesFraser -> ", bayesTest.filter(x => x._1 == 0.0 & x._2 == 1.0).count())


    val testVectors: RDD[Vector] = testDataMap.map { x => x.features }
    val predictResults: RDD[Double] = nbmodel.predict(testVectors)
//    println("Prediction Results:" + predictResults.


    // RANDOM FOREST MODEL
        println("Training Random Forest Regression Model...")
        val categoricalFeaturesInfo = Map[Int, Int]()
        val numClasses = 2
        val featureSubsetStrategy = "auto"
        val impurity = "variance"
        val maxDepth = 10
        val maxBins = 32
        val numTrees = 50
        val modelRF = RandomForest.trainRegressor(train, categoricalFeaturesInfo, numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)

        // Calculating random forest metrics
        val trainScores = train.map { point =>
          val prediction = modelRF.predict(point.features)
          (prediction, point.label)
        }
        val testScores = test.map { point =>
          val prediction = modelRF.predict(point.features)
          (prediction, point.label)
        }
        val metricsTrain = new BinaryClassificationMetrics(trainScores,100)
        val metricsTest = new BinaryClassificationMetrics(testScores,100)
        println("RF Training AuROC: ",metricsTrain.areaUnderROC())
        println("RF Test AuROC: ",metricsTest.areaUnderROC())

     //writing out ROC - don't forget to change the location of the ROC
        println("Attempting to write RF ROC to file...")
        val trainroc= metricsTrain.roc()
        val testroc= metricsTest.roc()
        trainroc.saveAsTextFile("rftrain")
        testroc.saveAsTextFile("rftest")

    // GRADIENT BOOSTED TREES REGRESSION MODEL
    //    println("Training Gradient Boosted Trees Regression Model...")
    //    val boostingStrategy = BoostingStrategy.defaultParams("Regression")
    //    boostingStrategy.numIterations = 50 // Note: Use more iterations in practice.
    //    boostingStrategy.treeStrategy.maxDepth = 5
    //    boostingStrategy.treeStrategy.categoricalFeaturesInfo = Map[Int, Int]()
    //    val modelGB = GradientBoostedTrees.train(train, boostingStrategy)
    //
    //    // Calculating gradient boosted metrics
    //    val trainScores2 = train.map { point =>
    //      val prediction = modelGB.predict(point.features)
    //      (prediction, point.label)
    //    }
    //    val testScores2 = test.map { point =>
    //      val prediction = modelGB.predict(point.features)
    //      (prediction, point.label)
    //    }
    //    val metricsTrain2 = new BinaryClassificationMetrics(trainScores2,100)
    //    val metricsTest2 = new BinaryClassificationMetrics(testScores2,100)
    //    println("GB Training AuROC: ",metricsTrain2.areaUnderROC())
    //    println("GB Test AuROC: ",metricsTest2.areaUnderROC())
    //
    //    // writing out ROC - don't forget to change the location of the ROC
    //    println("Attempting to write GB ROC to file...")
    //    val trainroc2= metricsTrain2.roc()
    //    val testroc2= metricsTest2.roc()
    //    trainroc2.saveAsTextFile("/Users/jblue/MOCKINGBIRD/ROC/gbtrain")
    //    testroc2.saveAsTextFile("/Users/jblue/MOCKINGBIRD/ROC/gbtest")


  }
}
