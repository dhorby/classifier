package com.springer.semantic.classifier.utils

import java.net.URL

import scala.io.Source._

class HttpUtils {

  def getLineList(urlStr: String) = {
    val openStream = new URL(urlStr).openStream()
    try {
      val lines = fromInputStream(openStream) ("utf-8").getLines().toList
      Some(lines)
    } catch {
      case e:Exception => e.printStackTrace()
        None
    } finally {
      openStream.close()
    }
  }

}
