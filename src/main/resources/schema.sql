drop table tuser if exists;

drop table model if exists;

create table  tuser (
    userId varchar(255) not null,
    admin bit not null,
    primary key (userId)
);

create table  model (
    modelId varchar(255) not null,
    modelPath varchar(255) not null,
    primary key (modelId)
);
