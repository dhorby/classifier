package com.springer.semantic.classifier.utils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Function;

import com.springer.semantic.classifier.utils.StopWordLists;
import org.apache.lucene.analysis.util.CharArraySet;

import com.springer.semantic.classifier.model.DataValue;
import com.springer.semantic.classifier.utils.io.ImportExport;

public class ConvertTSVToXML {

	public static void main(String[] args) {
		URL artFile = null;
		try {
			artFile = new URL("file://"  + "/Users/davidhorby/cluster/data/BMCAll-train.tsv");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File outputFile = new File("/Users/davidhorby/cluster/data/carrot/");
		ConvertTSVToXML convertTSVToXML = new ConvertTSVToXML();
		convertTSVToXML.runConvert(artFile, outputFile);
	}
	
	public void runConvert(URL tsvFile, File outputFile) {
		ImportExport ix = new ImportExport();
		try {
			ix.convertTSV(tsvFile, outputFile, ConvertTSVToXML.mapDataSet, StopWordLists.bmcStopSet);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Function<String, DataValue> mapDataSet = (line) -> {
		String[] p = line.split("	");
		try {
			return new DataValue(p[0], p[1], p[2]);
		} catch (ArrayIndexOutOfBoundsException ex ) {
			return new DataValue();
		}
	};	
	

}
