package com.springer.semantic.classifier.utils.io;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class StringSetUtils {

    public static HashSet<String> intersection(Set<String> set1, Set<String> set2) {
        HashSet<String> difference = new HashSet<String>(set1);
        difference.retainAll(set2);
        return difference;
    }

    public static HashSet<String> diff(Set<String> set1, Set<String> set2) {
        HashSet<String> difference = new HashSet<String>(set1);
        difference.removeAll(set2);
        return difference;
    }

    public static List<String> tokenizeSentences(final String text) {
        final List<String> strings = Arrays.asList(text.replaceAll("\\s+", " ").split(":|\\."));
        return strings.stream().map(e -> e.trim()).collect(Collectors.toList());
    }


    public static Set findCommonSentences(final List<Set<String>> sets) {
       return sets.stream().reduce(sets.get(0), (a, b) -> StringSetUtils.intersection(a, b));
    }

    public static String removeCommonSentences(Set<String> commonSentences, StringBuilder sentence) {
        for (String removeString : commonSentences) {
            sentence = new StringBuilder(sentence.toString().replace(removeString, ""));
        }
        return sentence.toString();
    }

}
