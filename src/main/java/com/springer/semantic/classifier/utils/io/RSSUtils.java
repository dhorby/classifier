package com.springer.semantic.classifier.utils.io;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


public class RSSUtils {

    private static final Logger LOG = LoggerFactory.getLogger(RSSUtils.class);

    public static Optional<List<SyndEntryImpl>> readRSS(URL feedUrl) {

        HttpURLConnection httpcon = null;
        try {
            httpcon = (HttpURLConnection) feedUrl.openConnection();
        } catch (IOException e) {
            LOG.error("Error Opening connection to " + feedUrl + " Error:" + e.getMessage());
            return Optional.empty();
        }
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = null;
        try {
            feed = input.build(new XmlReader(httpcon));
        } catch (Exception e) {
            LOG.error("Error parsing RSS Feed " + feedUrl + " Error:" + e.getMessage());
            e.printStackTrace();
            return Optional.empty();
        }
        List<SyndEntryImpl> entries = feed.getEntries();
        entries.forEach(entry -> System.out.println(" --->> [" + entry.getTitle() + "::" + extractBodyText(entry.getUri()) + ":" + entry.getDescription().getValue()));
        return Optional.of(entries);

    }

    public static String extractBodyText(String pageURL) {
        URL url = null;
        Document doc = null;
        try {
            url = new URL(pageURL);
            doc = Jsoup.parse(url, 3 * 1000);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc.body().text();
    }

    public static List<HashSet<String>> getCleanText(final String rssURl) throws MalformedURLException {

        RSSUtils ix = new RSSUtils();
        Optional<List<SyndEntryImpl>> entries = ix.readRSS(new URL(rssURl));
        Map<String, String> results = entries.get().stream().collect(Collectors.toMap(SyndEntryImpl::getTitle, e -> RSSUtils.extractBodyText(e.getLink())));
        Map<String, Set<String>> tokenizedMap = results.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> new HashSet(StringSetUtils.tokenizeSentences(e.getValue()))));
        final List<Set<String>> sets = tokenizedMap.entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());

        final Set<String> commonSentences = StringSetUtils.findCommonSentences(sets);

        final List<HashSet<String>> cleanSets = sets.stream().map(e -> StringSetUtils.diff(e, commonSentences)).collect(Collectors.toList());

        return cleanSets;

    }


}
