package com.springer.semantic.classifier.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.util.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HadoopUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HadoopUtils.class);

    public static Map<String, String> readSequenceFile(String sequenceFilePath, Configuration conf)  {
        Map<String, String> dataMap = new HashMap();
        try ( SequenceFile.Reader sequenceFileReader = new SequenceFile.Reader(FileSystem.get(conf), new Path(sequenceFilePath), conf)){
            Writable key = (Writable) ReflectionUtils.newInstance(sequenceFileReader.getKeyClass(), conf);
            Text value = new Text();
            while (sequenceFileReader.next(key, value)) {
                LOG.debug(sequenceFileReader.getPosition() + ":" + key  + ":" + value);
                dataMap.put(key.toString(), value.toString());
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return dataMap;
    }
}
