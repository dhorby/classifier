package com.springer.semantic.classifier.analyzer;


import com.springer.semantic.classifier.utils.StopWordLists;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.miscellaneous.KeepWordFilter;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;

import java.util.Arrays;
import java.util.List;

public class PhraseAnalyzer extends Analyzer {

    private static final List<String> phraseWords = Arrays.asList(
            "www.theguardian.com", "commentisfree", "href", "_", "http", "www.bbc.co.uk", "span", "https www.theguardian.com", "https", "related",
            "continue", "reading", "politics", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec", "p", "2016",
            "ul", "li", "related   a", "edition", "switch", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "eu"
    );
    public static final CharArraySet phraseSet = new CharArraySet(phraseWords, false);

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer source = new StandardTokenizer();
        TokenStream result = new StandardFilter(source);
        result = new EnglishPossessiveFilter(result);
        result = new LowerCaseFilter(result);
//        result = new StopFilter(result, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
        result = new StopFilter(result, phraseSet);
//        result = new KeepWordFilter(result, StopWordLists.phraseSet);
        ShingleFilter sf  =  new ShingleFilter(result, 3, 3);
//        sf.setFillerToken(" ");
        sf.setOutputUnigrams(false);
        result = sf;
//        TokenStream f = new BloomTokenFilter(getFilter(shingleKeepTokens),  true, sf);
//
//        result =  new CommonGramsFilter(result, StopWordLists.bmcStopSet);
        return new TokenStreamComponents(source, result);
    }

}
