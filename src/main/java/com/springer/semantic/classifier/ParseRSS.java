package com.springer.semantic.classifier;

import com.springer.semantic.classifier.model.DataValue;
import com.springer.semantic.classifier.utils.io.RSSUtils;
import com.sun.syndication.feed.synd.SyndEntryImpl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParseRSS {

    public static final String FOOTBALL_RSS = "http://feeds.bbci.co.uk/sport/football/rss.xml?edition=int";
    public static final String TENNIS_RSS = "http://feeds.bbci.co.uk/sport/tennis/rss.xml?edition=int";
    public static final String GOLF_RSS = "http://feeds.bbci.co.uk/sport/golf/rss.xml?edition=int";
    public static final String WORK_DIR = "/Users/davidhorby/hackday/bbcdata";



    public static void main(String[] args) {
        System.out.println("Started");

        try {
            new ParseRSS().doParse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished");


    }

    public static Function<String, Optional<Object>> mapBMCDataSet = (line) -> {
        String[] p = line.split("	");
        try {
            return Optional.of(new DataValue(p[0], p[1], p[2]));
        } catch (ArrayIndexOutOfBoundsException ex) {
            return Optional.empty();
        }
    };

    public void doParse() throws IOException {

        final Path path = Paths.get(WORK_DIR);
        final Path dataFilePath = Paths.get(WORK_DIR + "/bbc-data.txt");
        final File workDir = new File(WORK_DIR);
        final File sequenceDir = new File(WORK_DIR + "/sequence");


        URL tennisRSS = new URL(TENNIS_RSS);;
        URL footballRSS = new URL(FOOTBALL_RSS);;
        URL golfRSS = new URL(GOLF_RSS);

        final List<String> tennisLines = extractLines(tennisRSS, "tennis");
        final List<String> footballLines = extractLines(footballRSS, "football");
        final List<String> golfLines = extractLines(golfRSS, "golf");

        if (Files.exists(dataFilePath)) Files.delete(dataFilePath);
        Files.createFile(dataFilePath);


        Files.write(dataFilePath, tennisLines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
        Files.write(dataFilePath, footballLines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
        Files.write(dataFilePath, golfLines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);


//        final File[] files = workDir.listFiles();
//        for (int i = 0; i < files.length; i++) {
//            final File file = files[i];
//
//            new TfIdfUtils().createTfIdf(dataFilePath.toString());
//
//        }

//
//
//                ConvertToSeq convertToSeq = new ConvertToSeq();
//        convertToSeq.convert()


    }

    private List<String> extractLines(URL rssUrl, String tagValue) {

        Optional<List<SyndEntryImpl>> syndEntries = RSSUtils.readRSS(rssUrl);
        Map<String, String> results = syndEntries.get().stream().collect(Collectors.toMap(SyndEntryImpl::getTitle, e -> RSSUtils.extractBodyText(e.getLink())));

        final List<String> aLines = results.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        return aLines.stream().map(x -> tagValue + "\t" + x.hashCode() + "\t" + x).collect(Collectors.toList());
    }

}
