package com.springer.semantic.classifier.jetty.service;

import com.springer.semantic.classifier.utils.StopWordUtils;
import org.apache.lucene.analysis.Analyzer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TokenizeService {

    public Optional<List<String>> tokenize(String text, Analyzer analyzer) {
        return StopWordUtils.tokenizeList(text, analyzer);
    }
}
