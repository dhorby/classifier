package com.springer.semantic.classifier.jetty.web;

import com.google.common.base.Equivalence;
import com.springer.semantic.classifier.analyzer.PhraseAnalyzer;
import com.springer.semantic.classifier.jetty.service.PhraseService;
import com.springer.semantic.classifier.jetty.service.TokenizeService;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.commongrams.CommonGramsFilter;
import org.apache.mahout.ep.Mapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class TokenizeController {

    private static final Logger LOG = LoggerFactory.getLogger(TokenizeController.class);

    @Autowired
    private PhraseService phraseService;

    @RequestMapping(value = "/tokenize", method = RequestMethod.POST)
    @ResponseBody
    public String tokenize(@RequestParam(required = true) String textVal) {
        LOG.info("Tokenizing");
        List<PhraseService.WordCount> sortedWords = phraseService.getPhrases(textVal);
        String commaSeparatedWords = sortedWords.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(", "));
        return commaSeparatedWords;
    }
}