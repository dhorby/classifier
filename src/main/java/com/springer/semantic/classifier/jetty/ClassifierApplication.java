/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.springer.semantic.classifier.jetty;

import static com.springer.semantic.classifier.jetty.ClassifierApplication.basePath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@PropertySource("classpath:classifier.properties")
public class ClassifierApplication {
	
	
	//@Value("${basePath}")
	public static String basePath = "/Users/davidhorby/journals";
	public static String indexFile;
	
	public static String modelPath = "/journals/hadoop/model/";
	public static String labelIndexPath;
	public static String dictionaryPath;
	public static String documentFrequencyPath;		

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public static void main(String[] args) throws Exception {
		// New comment
		SpringApplication.run(ClassifierApplication.class, args);
		if (args.length > 0 && args[0] != null) {
			basePath = args[0];
		}
		if (args.length > 1 && args[1] != null) {
			indexFile = args[1];
		}		
		System.out.println("basePath:" + basePath);
		modelPath = basePath + "/hadoop/model";
		labelIndexPath = basePath + "/hadoop/labelindex";
		dictionaryPath = basePath + "/hadoop/article-vectors/dictionary.file-0";
		documentFrequencyPath = basePath + "/hadoop/article-vectors/df-count/part-r-00000";		
		System.out.println("documentFrequencyPath:" + documentFrequencyPath);

	}
	
}
