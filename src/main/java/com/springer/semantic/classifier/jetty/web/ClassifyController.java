/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.springer.semantic.classifier.jetty.web;


import java.util.ArrayList;
import java.util.TreeMap;

import com.springer.semantic.classifier.model.Journal;
import com.springer.semantic.classifier.model.Journals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.springer.semantic.classifier.jetty.service.TextClassifierService;
import com.springer.semantic.classifier.model.JournalScore;

@Controller
public class ClassifyController {

	private static final Logger LOG = LoggerFactory.getLogger(ClassifyController.class);

	@Autowired
	private TextClassifierService textClassifierService;

	@RequestMapping(value = "/classify", method = RequestMethod.POST)
	@ResponseBody
	public ArrayList<JournalScore> classify(@RequestParam(required = true) String abstractVal, @RequestParam(required = false) String resultCount) {
		LOG.info("Got request" + abstractVal);
		int resInt = 10;
		ArrayList<JournalScore> results = textClassifierService.classify(abstractVal, resInt);
		return results;
	}

	@RequestMapping(value = "/journals", method = RequestMethod.GET)
	@ResponseBody
	public TreeMap<String, Journal> journals() {
		return Journals.getInstance();
	}
}
