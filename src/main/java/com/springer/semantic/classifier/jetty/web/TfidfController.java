package com.springer.semantic.classifier.jetty.web;

import com.springer.semantic.classifier.utils.TfIdfUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;

@Controller
public class TfidfController {

    private static final Logger LOG = LoggerFactory.getLogger(TokenizeController.class);

    @RequestMapping(value = "/tfidf", method = RequestMethod.POST)
    @ResponseBody
    public String tfidf(@RequestParam(required = true) String textVal) {
        LOG.info("Generating Tf-IDF");
        try {
            File storedFile = File.createTempFile("stored-text", "txt");
            new TfIdfUtils().createTfIdf(storedFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Success";
    }
}