package com.springer.semantic.classifier.jetty.service;

import com.springer.semantic.classifier.analyzer.PhraseAnalyzer;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PhraseService {

    private static final Logger LOG = LoggerFactory.getLogger(PhraseService.class);

    @Autowired
    private TokenizeService tokenizeService;

    public class WordCount {

        private String word;
        private Long countVal;

        public WordCount(String word, Long countVal) {
            this.word = word;
            this.countVal = countVal;
        }

        public String getWord() {
            return this.word;
        }

        public Long getCountVal() {
            return this.countVal;
        }

        public String toString() {
            return this.word + ":" + this.countVal;
        }
    }

    Comparator<WordCount> byValue = (WordCount entry1, WordCount entry2) -> entry2.getCountVal().compareTo(entry1.getCountVal());

    public List<WordCount> getPhrases(String textVal) {
        List<String> phrases = tokenizeService.tokenize(textVal, new PhraseAnalyzer()).get();
        Map<String, Long> results = phrases.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        List<WordCount> words = results.entrySet().stream().map(p -> new WordCount(p.getKey(), p.getValue())).collect(Collectors.toList());
        List<WordCount> sortedWords = words.stream().sorted(byValue).collect(Collectors.toList());
        List<WordCount> filteredWords = sortedWords.stream().filter(phrase -> phrase.getCountVal() > 1 && phrase.getWord().replace("  ", " ").trim().split(" ").length > 1).collect(Collectors.toList());
//        filteredWords.forEach((wordCount) -> LOG.info("key:" + wordCount.getWord() + ":" + wordCount.getCountVal()));
        return filteredWords;
    }
}
