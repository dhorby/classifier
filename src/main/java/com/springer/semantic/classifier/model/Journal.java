package com.springer.semantic.classifier.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Journal implements Serializable {

	public Journal(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	private String id;
	private String name;
	private List<Word> words = new ArrayList<Word>();

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public List<Word> getWords() {
		return words;
	}

	public void setWords(List<Word> words) {
		this.words = words;
	}

}
