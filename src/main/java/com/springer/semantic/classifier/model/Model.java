package com.springer.semantic.classifier.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table; 


@Entity
public class Model implements Serializable {
	
	private static final long serialVersionUID = 1L;	

	@Id
	@Column 
	private String modelId;


	@Column(length = 255, nullable = true)
	private String modelPath;


	public String getModelId() {
		return modelId;
	}


	public void setModelId(String modelId) {
		this.modelId = modelId;
	}


	public String getModelPath() {
		return modelPath;
	}


	public void setModelPath(String modelPath) {
		this.modelPath = modelPath;
	}
	
	

}
