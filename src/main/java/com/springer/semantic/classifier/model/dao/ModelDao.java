package com.springer.semantic.classifier.model.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.springer.semantic.classifier.model.Model;

public interface ModelDao
{
    public Model findById( long id );
    public List<Model> findAll();
    public void save( Model customer );
    public void update( Model customer );
    public void delete( Model customer );
    public void shutdown();
}
