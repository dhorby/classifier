package com.springer.semantic.classifier.model;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.springer.semantic.classifier.jetty.ClassifierApplication;
import com.springer.semantic.classifier.utils.io.ImportExport;

public class Journals extends TreeMap<String, Journal> {

	private static final long serialVersionUID = 1L;
	private static volatile Journals instance = null;

	public static Function<String, Optional<Object>> mapJournals = (line) -> {
		String[] p = line.split("\t");
		try {
			return  Optional.of(new Journal(p[0], p[1]));
		} catch (ArrayIndexOutOfBoundsException ex ) {
			return Optional.empty();
		}
	};
	
	public static Journals getInstance() {
		if (instance == null) {
			synchronized (Journals.class) {
				if (instance == null) {
					instance = new Journals();
					try {
						instance.importJournals();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return instance;
	}

	public void importJournals() throws IOException {
		File inputFile = new File(getClass().getClassLoader().getResource("journals.tsv").getFile());
		List<Journal> journals = ImportExport.readTSV(inputFile, Journals.mapJournals).stream().filter(e -> e instanceof Journal).map(Journal.class::cast).collect(Collectors.toList());
		journals.forEach(journal -> this.put(journal.getId(), journal));
	}

	public String getJournalTitle(String journalID) {
		if (this.containsKey(journalID)) {
			return this.get(journalID).getName();
		}
		return "";
	}

}


