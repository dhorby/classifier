package com.springer.semantic.classifier.analyzer;

import com.springer.semantic.classifier.jetty.ClassifierApplication;
import com.springer.semantic.classifier.jetty.service.TokenizeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ClassifierApplication.class)
public class PhraseAnalyzerTest {

    @Autowired
    private TokenizeService tokenizeService;

    @Test
    public void testAanalyzer() {
        System.out.println("--->>>" + tokenizeService);
        List<String> phrases = tokenizeService.tokenize("The rain in Spain stays mainly on the plain", new PhraseAnalyzer()).get();
        System.out.println("--->>>" + phrases);
    }
}