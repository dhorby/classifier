package com.springer.semantic.classifier.utils.io;

import com.springer.semantic.classifier.model.DataValue;
import com.springer.semantic.classifier.utils.HadoopUtils;
import com.springer.semantic.classifier.utils.StopWordLists;
import com.springer.semantic.classifier.utils.StopWordUtils;
import com.springer.semantic.classifier.utils.TSVMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;


public class ImportExportTest {

    private static final Logger LOG = LoggerFactory.getLogger(ImportExportTest.class);

    public static final String BMCALL_TRAIN_ZIP = "bmc_train_test.zip";
    public static final String BMCALL_TRAIN_TSV = "bmc_train_test.tsv";
    public static final String BMCALL_TRAIN_PATH = "./data";
    public static final String HADOOP = "hadoop";


    @Test
    public void testConvertTSV() {

        EnglishAnalyzer englishAnalyzer = new EnglishAnalyzer(StopWordLists.bmcStopSet);
        File sourceFile = new File(this.getClass().getClassLoader().getResource(BMCALL_TRAIN_ZIP).getFile());
        ImportExport.unzip(sourceFile, BMCALL_TRAIN_PATH);
        File inputFile = new File(BMCALL_TRAIN_PATH, BMCALL_TRAIN_TSV);

        ImportExport ix = new ImportExport();
        Configuration configuration = new Configuration();
        int count = 0;
        try (SequenceFile.Writer writer = new SequenceFile.Writer(FileSystem.get(new Configuration()), configuration, new Path(HADOOP + "/chunk-0"),
                Text.class, Text.class)) {
            List<DataValue> dataFromTsv =  ImportExport.readTSV(inputFile, TSVMap.mapBMCDataSet).stream().filter(e -> e instanceof DataValue).map(DataValue.class::cast).collect(Collectors.toList());
            dataFromTsv.forEach(dataValue -> dataValue.setValue(StopWordUtils.tokenize(dataValue.getValue(), englishAnalyzer).get()));
            List<DataValue> dataFiltered = dataFromTsv.stream().filter(dataValue -> dataValue.getValue().length() > 20).collect(Collectors.toList());

            dataFiltered.forEach(dataValue -> dataValue.setKey("/" + dataValue.getCategory() + "/" + dataValue.getKey()));
            for (DataValue dataValue : dataFiltered) {
                writer.append(new Text(dataValue.getKey()), new Text(dataValue.getValue()));
                LOG.info(dataValue.getKey() + ":" + dataValue.getValue());
                count++;
            }
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        // Now read the sequence
        Map<String, String> dataMap = HadoopUtils.readSequenceFile(HADOOP + "/chunk-0", configuration);
        assertTrue(dataMap.size() > 0);
    }



}