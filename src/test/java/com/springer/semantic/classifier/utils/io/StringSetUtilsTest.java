package com.springer.semantic.classifier.utils.io;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.springer.semantic.classifier.utils.io.StringSetUtils.tokenizeSentences;


public class StringSetUtilsTest {

    public static final String TEST_TEXT1 = "Author Fred Smith. The rain in Spain stays mainly on the plain. There is a cow in the field. All rights reserved. I    love cheese";
    public static final String TEST_TEXT2 = "Author Fred Smith. Yesterday it was raining. Today it is sunny. All rights reserved. I  love cheese";
    public static final String TEST_TEXT3 = "Author Fred Smith. Yesterday it was raining. Today it is sunny. All rights reserved. I  love cheese";

    @Test
    public void testFindCommonSentences() {
        HashSet<String> expectedResult = new HashSet<String>(Arrays.asList("Author Fred Smith", "I love cheese", "All rights reserved"));
        HashSet sentences1 = new HashSet(tokenizeSentences(TEST_TEXT1));
        HashSet sentences2 = new HashSet(tokenizeSentences(TEST_TEXT2));
        HashSet sentences3 = new HashSet(tokenizeSentences(TEST_TEXT3));
        ArrayList<HashSet> sets = new ArrayList<HashSet>();
        sets.add(sentences1);
        sets.add(sentences2);
        sets.add(sentences3);
        HashSet reduceResult = sets.stream().reduce(sentences1, (a, b) -> StringSetUtils.intersection(a, b));
        Assert.assertArrayEquals(reduceResult.toArray(), expectedResult.toArray());
    }

    @Test
    public void testTokenizeSentences() {
        List expectedResult = Arrays.asList("Author Fred Smith", "The rain in Spain stays mainly on the plain", "There is a cow in the field", "All rights reserved", "I love cheese");
        List<String> sentences1 = tokenizeSentences(TEST_TEXT1);
        Assert.assertTrue(sentences1.equals(expectedResult));
    }
}