package com.springer.semantic.classifier.utils;

import com.springer.semantic.classifier.jetty.ClassifierApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.springer.semantic.classifier.utils.io.ImportExport.readTSV;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ClassifierApplication.class)
public class ConvertToSeqTest {

	private static final Logger LOG = LoggerFactory.getLogger(ConvertToSeqTest.class);

	@Test
	public void testReadTSV() {
		File inputFile = new File(this.getClass().getClassLoader().getResource("test-train.tsv").getFile());
		ConvertToSeq convertToSeq = new ConvertToSeq();
		List<Object> dataVals = null;
		try {
			dataVals = readTSV(inputFile, TSVMap.mapBMCDataSet);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
		assertEquals(19 ,dataVals.size());
	}




}
